<?php
namespace silenca\Mailchimp;

use silenca\Mailchimp\Exception\Init as InitException;
use silenca\Mailchimp\Exception\Query as QueryException;

class Client implements ClientInterface
{
    /**
     * API-ключ
     *
     * @var string
     */
    private $key;

    /**
     * URL точки входа
     *
     * @var string
     */
    private $endpoint;


    /**
     * Фабрика ответов сервера Mailchimp
     *
     * @var Client\Response\FactoryInterface
     */
    private $responseFactory;

    /**
     * Шаблон URL точки фхода
     */
    const ENDPOINT_TEMPLATE = 'https://<dc>.api.mailchimp.com/3.0/';

    public function __construct($key)
    {
        if(!$key) {
            throw new InitException('No key specified', InitException::EC_NO_KEY);
        }
        $this->key = $key;
    }

    /**
     * Отправка запроса
     *
     * @param \silenca\Mailchimp\Client\RequestInterface $request
     * @return \silenca\Mailchimp\Client\ResponseInterface
     */
    public function query(Client\RequestInterface $request)
    {
        $url = $this->getEndpoint($request->getPath(), $request->getQueryExtras());
        $params = $request->getParams();

        $headers = $request->getHeaders();
        $headers[] = $this->getAuthHeaderString();
        $headers[] = 'Content-type: application/json';

        $options = array(
            'http' => array(
                'header'  => implode("\r\n", $headers),
                'method'  => $request->getMethod(),
                'content' => json_encode($params),
                'ignore_errors' => true,
            ),
        );

        $context = stream_context_create($options);

        $result = file_get_contents($url, false, $context);

        $result = json_decode($result, true);
        if(false === $request) {
            throw new QueryException('Not an JSON response', QueryException::EC_WRONG_REQUEST_FORMAT);
        }

        return $this->getResponseFactory()->build($result, $http_response_header);
    }

    /**
     * Возвращает точку входа с учетом REST-пути
     *
     * @param string $path
     * @return string
     */
    private function getEndpoint($path = null, array $extras = array())
    {
        if(!isset($this->endpoint)) {
            $this->endpoint = str_replace('<dc>', $this->getDc(), self::ENDPOINT_TEMPLATE);
        }

        $endpoint = $this->endpoint;

        if($path) {
            $endpoint .= ltrim($path, '/').'/';
        }
        
        if(count($extras)) {
            $endpoint .= '?'.http_build_query($extras);
        }
        
        return $endpoint;
    }

    /**
     * Вытаскивает код DataCenter из API-ключа MailChimp
     *
     * @return string
     */
    private function getDc()
    {
        return preg_replace('/^(.+)-([a-z0-9]+)$/', '$2', $this->getKey());
    }

    /**
     * Возвращает ключ авторизации
     *
     * @return string
     */
    private function getKey()
    {
        return $this->key;
    }

    /**
     * Возвращает строку HTTP-заголовка для авторизации
     *
     * @return string
     */
    private function getAuthHeaderString()
    {
        return 'Authorization: api-key '.$this->getKey();
    }

    public function getResponseFactory()
    {
        if(!isset($this->responseFactory)) {
            throw new \Exception();
        }
        return $this->responseFactory;
    }

    public function setResponseFactory(Client\Response\FactoryInterface $factory)
    {
        $this->responseFactory = $factory;
        return $this;
    }
}