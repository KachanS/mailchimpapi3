<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Report extends AbstractRequest
{
    protected $camaping;

    public function __construct(\silenca\Mailchimp\Model\Campaigns\Instance $campaign)
    {
        $this->camaping = $campaign;
    }

    public function getPath()
    {
        return 'reports/'.$this->getCampaignId().'';
    }

    public function setParams(array $params)
    {
        throw new \Exception('Not implemented');
    }

    public function getCampaign()
    {
        return $this->camaping;
    }

    public function getCampaignId()
    {
        return $this->getCampaign()['id'];
    }
}