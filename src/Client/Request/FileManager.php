<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class FileManager extends AbstractRequest
{
    public function getPath()
    {
        return 'file-manager';
    }
}