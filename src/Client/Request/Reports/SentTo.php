<?php
namespace silenca\Mailchimp\Client\Request\Reports;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class SentTo extends AbstractRequest
{
    protected $report;

    public function __construct(\silenca\Mailchimp\Model\Reports\Instance $report)
    {
        $this->report = $report;
    }

    public function getPath()
    {
        return 'reports/'.$this->getReportId().'/sent-to';
    }

    public function setParams(array $params)
    {
        throw new \Exception('Not implemented');
    }

    public function getReport()
    {
        return $this->report;
    }

    public function getReportId()
    {
        return $this->getReport()['id'];
    }
}