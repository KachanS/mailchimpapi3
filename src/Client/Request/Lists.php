<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Lists extends AbstractRequest
{
    public function getPath()
    {
        return 'lists';
    }
    
    public function getQueryExtras()
    {
        return array(
            'sort_field' => 'send_time',
            'sort_dir' => 'DESC',
        );
    }
}