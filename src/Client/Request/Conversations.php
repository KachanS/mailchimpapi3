<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Conversations extends AbstractRequest
{
    public function getPath()
    {
        return 'conversations';
    }
}