<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Campaigns extends AbstractRequest
{
    public function getPath()
    {
        return 'campaigns';
    }
    
    public function getQueryExtras()
    {
        return array(
            'sort_field' => 'send_time',
            'sort_dir' => 'DESC',
        );
    }
}