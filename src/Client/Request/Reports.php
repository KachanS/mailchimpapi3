<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Reports extends AbstractRequest
{
    public function getPath()
    {
        return 'reports';
    }
}