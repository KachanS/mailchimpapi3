<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class AuthorizedApps extends AbstractRequest
{
    public function getPath()
    {
        return 'authorized-apps';
    }
}