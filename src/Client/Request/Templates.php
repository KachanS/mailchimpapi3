<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Templates extends AbstractRequest
{
    public function getPath()
    {
        return 'templates';
    }
}