<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Automations extends AbstractRequest
{
    public function getPath()
    {
        return 'automations';
    }
}