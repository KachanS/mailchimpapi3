<?php
namespace silenca\Mailchimp\Client\Request\Lists;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Create extends AbstractRequest
{
    public function __construct(\silenca\Mailchimp\Model\Lists\Instance $list)
    {
        //$this->params = $this->getDefaultParams();
        $this->setList($list);
    }

    public function getPath()
    {
        return 'lists';
    }

    public function getMethod()
    {
        return self::METHOD_POST;
    }

    public function setList(\silenca\Mailchimp\Model\Lists\Instance $list)
    {
        foreach($list as $field=>$value) {
            $this->setParam($field, $value);
        }
        return $this;
    }

    public function setParams(array $params)
    {
        throw new \Exception('Not implemented');
    }

    private function getDefaultParams()
    {
        return array(
            'name' => '',
            'contact' => array(
                'company' => 'a',
                'address1' => 'b',
                'address2' => 'c',
                'city' => 'd',
                'state' => 'e',
                'zip' => 'f',
                'country' => 'g',
                'phone' => 'h',
            ),
            'permission_reminder' => '0',
            'campaign_defaults' => array(
                'from_name' => 'The from name',
                'from_email' => 'from@email.com',
                'subject' => 'The subject',
                'language' => 'The language',
            ),
            'notify_on_subscribe' => false,
            'notify_on_unsubscribe' => false,
            'email_type_option' => false,
            'visibility' => \silenca\Mailchimp\Model\Lists\Instance::VISIBILITY_PUBLIC,
        );
    }
}