<?php
namespace silenca\Mailchimp\Client\Request\Lists;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class MergeFields extends AbstractRequest
{
    protected $list;
    
    public function __construct(\silenca\Mailchimp\Model\Lists\Instance $list)
    {
        $this->setList($list);
    }

    public function getPath()
    {
        return 'lists/'.$this->getListId().'/merge-fields';
    }

    public function setList(\silenca\Mailchimp\Model\Lists\Instance $list)
    {
        $this->list = $list;
        return $this;
    }

    public function getList()
    {
        return $this->list;
    }

    public function getListId()
    {
        return $this->getList()['id'];
    }
}