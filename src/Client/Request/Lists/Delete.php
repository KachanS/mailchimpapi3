<?php
namespace silenca\Mailchimp\Client\Request\Lists;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Delete extends AbstractRequest
{
    protected $list;

    public function __construct(\silenca\Mailchimp\Model\Lists\Instance $list)
    {
        $this->list = $list;
    }

    public function getPath()
    {
        return 'lists/'.$this->getListId();
    }

    public function getMethod()
    {
        return self::METHOD_DELETE;
    }

    public function setParams(array $params)
    {
        throw new \Exception('Not implemented');
    }
    
    public function setParam($name, $value)
    {
        throw new \Exception('Not implemented');
    }

    public function getList()
    {
        return $this->list;
    }

    public function getListId()
    {
        return $this->getList()['id'];
    }
}