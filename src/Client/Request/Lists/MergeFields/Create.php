<?php
namespace silenca\Mailchimp\Client\Request\Lists\MergeFields;

use silenca\Mailchimp\Client\Request as AbstractRequest;
use silenca\Mailchimp\Model\Lists\Instance as ListInstance;
use silenca\Mailchimp\Model\MergeFields\Instance as FieldInstance;

class Create extends AbstractRequest
{
    protected $list = null;
    protected $field = null;

    public function __construct(ListInstance $list, FieldInstance $field)
    {
        $this->field = $field;
        $this->list = $list;
    }

    public function getPath()
    {
        return 'lists/'.$this->getListId().'/merge-fields';
    }

    public function getMethod()
    {
        return self::METHOD_POST;
    }

    public function getList()
    {
        return $this->list;
    }

    public function getListId()
    {
        return $this->getList()['id'];
    }

    public function setParams(array $params)
    {
        throw new \Exception('Not implemented');
    }

    public function getParams()
    {
        return (array) $this->field;
    }
}