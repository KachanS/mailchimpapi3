<?php
namespace silenca\Mailchimp\Client\Request\Lists\Members;

use silenca\Mailchimp\Client\Request as AbstractRequest;
use silenca\Mailchimp\Model\Lists\Instance as ListInstance;
use silenca\Mailchimp\Model\Members\Instance as MemberInstance;
use silenca\Mailchimp\Model\Members\Collection as MemberCollection;

class Create extends AbstractRequest
{
    protected $list = null;
    protected $member = null;

    public function __construct(ListInstance $list, MemberInstance $member)
    {
        $this->member = $member;
        $this->list = $list;
    }

    public function getPath()
    {
        return 'lists/'.$this->getListId().'/members';
    }

    public function getMethod()
    {
        return self::METHOD_POST;
    }

    public function getList()
    {
        return $this->list;
    }

    public function getListId()
    {
        return $this->getList()['id'];
    }

    public function setParams(array $params)
    {
        throw new \Exception('Not implemented');
    }

    public function getParams()
    {
        return (array) $this->member;
    }
}