<?php
namespace silenca\Mailchimp\Client\Request\Lists\Members;

use silenca\Mailchimp\Client\Request as AbstractRequest;
use silenca\Mailchimp\Model\Lists\Instance as ListInstance;
use silenca\Mailchimp\Model\Members\Instance as MemberInstance;
use silenca\Mailchimp\Model\Members\Collection as MemberCollection;

class BatchCreate extends AbstractRequest
{
    protected $list = null;
    protected $members = array();

    public function __construct(ListInstance $list, array $members = array())
    {
        foreach($members as $member) {
            if($member instanceof MemberInstance) {
                $this->addMember($member);
            }
        }
        $this->list = $list;
    }

    public function getPath()
    {
        return 'lists/'.$this->getListId().'/members';
    }

    public function getMethod()
    {
        return self::METHOD_POST;
    }

    public function getList()
    {
        return $this->list;
    }

    public function getListId()
    {
        return $this->getList()['id'];
    }

    public function setParams(array $params)
    {
        throw new \Exception('Not implemented');
    }

    public function addMember(MemberInstance $member)
    {
        $this->members[] = $member;
        return $this;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function getParams()
    {
        return array(
            'list_id' => $this->getListId(),
            'members' => array_map(function($member) { return array(
                'email_address' => $member->email_address,
                'email_type' => 'html',
                'status' => 'subscribed',
                'status_if_new' => 'subscribed',
                'merge_fields' => (array) $member,
                'list_id' => $this->getListId(),
            ); }, $this->getMembers()),
        );
    }
}