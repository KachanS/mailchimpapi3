<?php
namespace silenca\Mailchimp\Client\Request;

use silenca\Mailchimp\Client\Request as AbstractRequest;

class Batch extends AbstractRequest
{
    protected $requests = array();

    public function addRequest(AbstractRequest $request)
    {
        $this->requests[] = $request;
        return $this;
    }

    public function getMethod()
    {
        return self::METHOD_POST;
    }

    public function getRequests()
    {
        return $this->requests;
    }

    public function getPath()
    {
        return 'batches';
    }

    public function getParams()
    {
        return array(
            'operations' => array_map(function($request) {
                return array(
                    'method' => $request->getMethod(),
                    'path' => '/'.$request->getPath(),
                    'params' => '{}',
                    'body' => json_encode($request->getParams()),
                );
            }, $this->getRequests()),
        );
    }
}