<?php
namespace silenca\Mailchimp\Client\Response\Reports\Sentto;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Collection extends AbstractResponse
{
    protected $collection;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);
        
        foreach($data['sent_to'] as $listData) {
            $this->getCollection()->add(new \silenca\Mailchimp\Model\Sentto\Instance($listData));
        }
    }

    /**
     *
     * @return \silenca\Mailchimp\Model\Sentto\Collection
     */
    public function getCollection()
    {
        if(!isset($this->collection)) {
            $this->collection = new \silenca\Mailchimp\Model\Sentto\Collection();
        }
        return $this->collection;
    }
}