<?php
namespace silenca\Mailchimp\Client\Response\Reports;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Collection extends AbstractResponse
{
    protected $collection;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);
        
        foreach($data['reports'] as $listData) {
            $this->getCollection()->add(new \silenca\Mailchimp\Model\Reports\Instance($listData));
        }
    }

    /**
     *
     * @return \silenca\Mailchimp\Model\Reports\Collection
     */
    public function getCollection()
    {
        if(!isset($this->collection)) {
            $this->collection = new \silenca\Mailchimp\Model\Reports\Collection();
        }
        return $this->collection;
    }
}