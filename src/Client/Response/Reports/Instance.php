<?php
namespace silenca\Mailchimp\Client\Response\Reports;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Instance extends AbstractResponse
{
    protected $instance = null;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);

        $this->instance = new \silenca\Mailchimp\Model\Reports\Instance($data);
    }

    public function getInstance()
    {
        return $this->instance;
    }
}