<?php
namespace silenca\Mailchimp\Client\Response\Reports\EmailActivity;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Collection extends AbstractResponse
{
    protected $collection;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);
        
        foreach($data['emails'] as $listData) {
            $this->getCollection()->add(new \silenca\Mailchimp\Model\EmailActivity\Instance($listData));
        }
    }

    /**
     *
     * @return \silenca\Mailchimp\Model\EmailActivity\Collection
     */
    public function getCollection()
    {
        if(!isset($this->collection)) {
            $this->collection = new \silenca\Mailchimp\Model\EmailActivity\Collection();
        }
        return $this->collection;
    }
}