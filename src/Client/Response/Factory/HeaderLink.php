<?php
namespace silenca\Mailchimp\Client\Response\Factory;

use silenca\Mailchimp\Client\Response\FactoryInterface;

class HeaderLink implements FactoryInterface
{
    private $response_class_prefix;

    public function build(array $data = array(), array $headers = array())
    {
        $link = false;
        foreach($headers as $headerString) {
            $matches = array();
            $parts = array_filter(array_map('trim', explode(';', $headerString)));
            foreach($parts as $part) {
                @list($name, $value) = array_filter(array_map('trim', explode(': ', $part)));
                $name = strtolower($name);
                if($name == 'link') {
                    $link = $value;
                }
            }
            if($link) {
                break;
            }
        }
        if($link) {
            $link = trim($link, '<>');
            $linkData = array_filter(array_map('trim', explode('/', $link)));

            foreach($linkData as $k=>$linkPart) {
                unset($linkData[$k]);
                if($linkPart == '3.0') {
                    break;
                }
            }

            foreach($linkData as $k=>$linkPart) {
                $linkData[$k] = str_replace('.json', '', $linkPart);
            }

            $linkAlias = implode('.', array_map('strtolower', $linkData));
        }

        $resposeClassName = $this->buildClassNameFromAlias($linkAlias);
        if(!class_exists($resposeClassName)) {
            $resposeClassName = '\\silenca\\Mailchimp\\Client\\Response\\Simple';
        }

        return new $resposeClassName($data, $headers);
    }

    private function getResponseClassPrefix()
    {
        if(!isset($this->response_class_prefix)) {
            $namespaceData = array_filter(explode('\\', __NAMESPACE__));
            array_pop($namespaceData);
            $this->response_class_prefix = '\\'.implode('\\', $namespaceData).'\\';
        }
        return $this->response_class_prefix;
    }

    private function buildClassNameFromAlias($alias)
    {
        $aliasData = array_map(function($item){
            return implode('', array_map('ucfirst', explode('-', $item)));
        }, explode('.', $alias));

        return $this->getResponseClassPrefix().implode('\\', $aliasData);
    }
}