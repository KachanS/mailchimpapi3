<?php
namespace silenca\Mailchimp\Client\Response\Lists;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Collection extends AbstractResponse
{
    protected $listsCollection;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);

        foreach($data['lists'] as $listData) {
            $this->getCollection()->add(new \silenca\Mailchimp\Model\Lists\Instance($listData));
        }
    }

    /**
     *
     * @return \silenca\Mailchimp\Model\Lists\Collection
     */
    public function getCollection()
    {
        if(!isset($this->listsCollection)) {
            $this->listsCollection = new \silenca\Mailchimp\Model\Lists\Collection();
        }
        return $this->listsCollection;
    }
}