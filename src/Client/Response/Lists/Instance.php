<?php
namespace silenca\Mailchimp\Client\Response\Lists;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Instance extends AbstractResponse
{
    protected $instance = null;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);

        $this->instance = new \silenca\Mailchimp\Model\Lists\Instance($data);
    }

    public function getInstance()
    {
        return $this->instance;
    }
}