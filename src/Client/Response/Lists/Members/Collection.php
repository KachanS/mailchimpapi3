<?php
namespace silenca\Mailchimp\Client\Response\Lists\Members;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Collection extends AbstractResponse
{
    protected $collection;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);

        foreach($data['members'] as $memberData) {
            $this->getCollection()->add(new \silenca\Mailchimp\Model\Members\Instance($memberData));
        }
    }

    /**
     *
     * @return \silenca\Mailchimp\Model\Members\Collection
     */
    public function getCollection()
    {
        if(!isset($this->collection)) {
            $this->collection = new \silenca\Mailchimp\Model\Members\Collection();
        }
        return $this->collection;
    }
}