<?php
namespace silenca\Mailchimp\Client\Response\Lists\MergeFields;

use silenca\Mailchimp\Client\Response as AbstractResponse;

class Collection extends AbstractResponse
{
    protected $collection;

    public function __construct(array $data = array(), array $headers = array())
    {
        parent::__construct($data, $headers);

        foreach($data['merge-fields'] as $fieldData) {
            $this->getCollection()->add(new \silenca\Mailchimp\Model\MergeFields\Instance($fieldData));
        }
    }

    /**
     *
     * @return \silenca\Mailchimp\Model\MergeFields\Collection
     */
    public function getCollection()
    {
        if(!isset($this->collection)) {
            $this->collection = new \silenca\Mailchimp\Model\MergeFields\Collection();
        }
        return $this->collection;
    }
}