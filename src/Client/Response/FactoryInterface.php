<?php
namespace silenca\Mailchimp\Client\Response;

interface FactoryInterface
{
    public function build(array $data = array(), array $headers = array());
}