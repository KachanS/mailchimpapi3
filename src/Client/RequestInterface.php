<?php
namespace silenca\Mailchimp\Client;

interface RequestInterface
{
    public function getHeaders();
    public function getPath();
    public function getMethod();
    public function getParams();
    public function getQueryExtras();
}