<?php
namespace silenca\Mailchimp\Client;

abstract class Response implements ResponseInterface
{
    protected $data = array();
    protected $headers = array();
    protected $status;
    protected $isError = false;

    public function __construct(array $data = array(), array $headers = array())
    {
        //HTTP/1.0 400 Bad Request
        $status = false;
        foreach($headers as $header) {
            $matches = array();
            if(preg_match('/^(HTTP\/[\.0-9]+) +([0-9]{3}) +(.+)$/', $header, $matches)) {
                $status = $matches[2];
            }
            if($status) break;
        }

        if(!$status) {
            throw new \Exception('Can not determine status');
        }

        if(in_array(floor($status/100), array(4, 5))) {
            $this->isError = true;
        }

        $this->status = $status;
        $this->data = $data;
        $this->headers = $headers;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function __call($name, $arguments)
    {
        $matches = array();
        if(preg_match('/^get(.+)$/', $name, $matches)) {
            $fieldName = implode('_', array_map('strtolower', array_filter(preg_split('/(?=[A-Z]+)/', $matches[1]))));
            
            return isset($this->data[$fieldName])?$this->data[$fieldName]:null;
        }
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function isError()
    {
        return $this->isError;
    }
}