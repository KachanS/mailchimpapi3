<?php
/**
 * Абстрактный запрос
 * 
 */
namespace silenca\Mailchimp\Client;

abstract class Request implements RequestInterface
{
    /**
     * Константы методов запроса
     */
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';

    /**
     * Параметры запроса
     * 
     * @var mixed[]
     */
    protected $params = array();

    /**
     * Заголовки запроса
     *
     * @return string[]
     */
    final public function getHeaders()
    {
        return array();
    }

    /**
     * Метод запроса
     *
     * @return string
     */
    public function getMethod()
    {
        return self::METHOD_GET;
    }

    /**
     * Установка параметров запроса
     *
     * @param array $params
     * @return \silenca\Mailchimp\Client\Request
     */
    public function setParams(array $params)
    {
        foreach($params as $name=>$value) {
            $this->setParam($name, $value);
        }
        return $this;
    }

    /**
     * Установка параметра запроса
     *
     * @param string $name
     * @param string|array $value
     * @return \silenca\Mailchimp\Client\Request
     */
    public function setParam($name, $value)
    {
        $this->params[$name] = $value;
        return $this;
    }

    /**
     * Получение значения параметра запроса
     *
     * @param string $name
     * @param mixed $default Значение, если параметр не установлен
     * @return mixed
     */
    public function getParam($name, $default = null)
    {
        return $this->hasParam($name)?$this->params[$name]:$default;
    }

    /**
     * Проверка существования параметра
     *
     * @param string $name
     * @return mixed
     */
    public function hasParam($name)
    {
        return isset($this->params[$name]);
    }

    /**
     * Получение параметров запроса
     *
     * @return mixed[]
     */
    public function getParams()
    {
        return $this->params;
    }
    
    /**
     *  Дополнительные параметры для URL
     * 
     * @return type
     */
    public function getQueryExtras()
    {
        return array();
    }
}