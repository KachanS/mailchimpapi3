<?php
namespace silenca\Mailchimp\Client;

interface ResponseInterface
{
    public function getData();
    public function getHeaders();
}