<?php
namespace silenca\Mailchimp;

interface ClientInterface
{
    public function query(Client\RequestInterface $request);
}