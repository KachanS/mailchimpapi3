<?php
namespace silenca\Mailchimp\Model;

abstract class Collection implements \Iterator, \Countable
{
    protected $items = array();
    protected $key = 0;

    /**
     *
     * @return Instance
     */
    public function current()
    {
        return $this->items[$this->key];
    }

    public function key()
    {
        return $this->key;
    }

    public function next()
    {
        $this->key++;
    }

    public function rewind()
    {
        $this->key = 0;
    }

    public function valid()
    {
        return isset($this->items[$this->key]);
    }

    public function count($mode = 'COUNT_NORMAL')
    {
        return count($this->items);
    }

    /**
     * Добавление списка в коллекцию
     *
     * @param Instance $list
     * @return Collection
     */
    public function add(Instance $list)
    {
        $this->items[] = $list;
        return $this;
    }

    /**
     * Проверка существования списка по ID
     *
     * @param type $id
     * @return type
     */
    public function has($id)
    {
        return !!$this->findBy('id', $id);
    }

    /**
     * Ищем список по параметру
     *
     * @param string $field Field name to search for
     * @param type $value Value to compare
     * @return Instance
     */
    public function findBy($field, $value)
    {
        return current(array_filter($this->items, function($list) use($field, $value){
            $fieldPath = explode('/', $field);
            $data = (array) $list;
            $lastKey = array_pop($fieldPath);

            foreach($fieldPath as $item) {
                if(is_array($data[$item])) {
                    $data = $data[$item];
                } else {
                    $data = null;
                }
            }
            
            if(is_array($data)) {
                return $data[$lastKey] == $value;
            }
            return false;
        }));
    }
}