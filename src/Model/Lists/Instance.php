<?php
namespace silenca\Mailchimp\Model\Lists;

class Instance extends \silenca\Mailchimp\Model\Instance
{
    const VISIBILITY_PUBLIC = 'pub';
    const VISIBILITY_PRIVATE = 'prv';
}