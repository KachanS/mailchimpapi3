<?php
namespace silenca\Mailchimp\Model\Campaigns;

class Instance extends \silenca\Mailchimp\Model\Instance
{
    const STATUS_SAVED = 'save';
    const STATUS_PAUSED = 'paused';
    const STATUS_SCHEDULED = 'schedule';
    const STATUS_SENDING = 'sending';
    const STATUS_SENT = 'sent';
    const STATUS_CANCELED = 'canceled';

    const TYPE_REGULAR = 'regular';
    const TYPE_PLAINTEXT = 'plain-text';
    const TYPE_ABSPLIT = 'absplit';
    const TYPE_RSS = 'rss';
}