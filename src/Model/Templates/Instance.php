<?php
namespace silenca\Mailchimp\Model\Templates;

class Instance extends \silenca\Mailchimp\Model\Instance
{
    const TYPE_USER = 'user';
    const TYPE_BASE = 'base';
    const TYPE_GALLERY = 'gallery';
}