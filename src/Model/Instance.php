<?php
namespace silenca\Mailchimp\Model;

use ArrayAccess;

abstract class Instance implements ArrayAccess
{
    public function __construct(array $data = array())
    {
        $this->merge($data);
    }

    public function offsetExists($offset)
    {
        return isset($this->$offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->$offset);
    }

    public function merge(array $data = array())
    {
        foreach($data as $k=>$v) {
            $this[$k] = $v;
        }
        return $this;
    }

    public function __get($name)
    {
        
    }
}